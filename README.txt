﻿##################
# Веб-сайт: EXTRIM NAVIGATOR
##################
# Дата создания: октябрь 2016
##################
# Команда разработчиков: Simple Solutions
##################


Сайт предназначен для поиска и добавления на карте  мест для занятий экстримом с их описанием и фото.
Для зарегестрированных пользователей есть возможность оставлять комментарии, оценивать, добавлять свои записи.


##################
# ВАЖНЫЕ ЗАМЕЧАНИЯ
##################
Вёрстка на HTML5 и CSS3.
Поддержку браузерами можно посмотреть на http://caniuse.com/#search=html5.
Проверка веб-страниц поддержки браузерами  осуществляется  с помощью Modernizr.


